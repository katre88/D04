package ee.bcs.valiit;

import java.util.Arrays;
import java.util.Scanner;

public class Yl17 {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        System.out.println("Sisesta numbrid (0-9), numbrite vahele lisa tühikud.");
        char[] inputArray = input.nextLine().toCharArray();

        for (int i = 0; i < inputArray.length; i++) {
            char checkNr = inputArray[i];
            switch (checkNr) {
                case '0':
                    System.out.print("null");
                    break;
                case '1':
                    System.out.print("üks");
                    break;
                case '2':
                    System.out.print("kaks");
                    break;
                case '3':
                    System.out.print("kolm");
                    break;
                case '4':
                    System.out.print("neli");
                    break;
                case '5':
                    System.out.print("viis");
                    break;
                case '6':
                    System.out.print("kuus");
                    break;
                case '7':
                    System.out.print("seitse");
                    break;
                case '8':
                    System.out.print("kaheksa");
                    break;
                case '9':
                    System.out.print("üheksa");
                    break;
                case ' ':
                    System.out.print(", ");
                    break;
                default:
                    System.out.print("Proovi uuesti");
                    break;


            }
        }


    }
}
