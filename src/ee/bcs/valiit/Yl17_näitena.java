package ee.bcs.valiit;

public class Yl17_näitena {
    public static void main(String[] args) {

        StringBuilder resultaat = new StringBuilder();
        for (int i = 0; i < args.length; i++) {
            switch (args[i]) {
                case "0":
                    resultaat.append("null");
                    break;
                case "1":
                    resultaat.append("üks");
                    break;
                case "2":
                    resultaat.append("kaks");
                    break;
                case "3":
                    resultaat.append("kolm");
                    break;
                case "4":
                    resultaat.append("neli");
                    break;
                case "5":
                    resultaat.append("viis");
                    break;
                case "6":
                    resultaat.append("kuus");
                    break;
                case "7":
                    resultaat.append("seitse");
                    break;
                case "8":
                    resultaat.append("kaheksa");
                    break;
                case "9":
                    resultaat.append("üheksa");
                    break;
                default:
                    break;
            }

            if (args.length > i + 1) {
                resultaat.append(", ");

            }

        }
        System.out.println(resultaat);
    }
}