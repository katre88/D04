package ee.bcs.valiit;

import java.util.HashMap;
import java.util.Map;

public class Yl22 {
    public static void main(String[] args) {
        HashMap<String, String[]> linnad = new HashMap<>();
        linnad.put("Estonia", new String[]{"Tallinn", "Tartu", "Valga", "Võru"});
        linnad.put("Sweden", new String[]{"Stockholm", "Uppsala", "Lund", "Köping"});
        linnad.put("Finland", new String[]{"Helsinki", "Espoo", "Hanko", "Jämsä"});

        for (int i = linnad.size()-1 ; i>=0; i--) {
            System.out.println("Country: " + linnad.keySet().toArray()[i]);
            System.out.println("Cities:");
            for (int j=0; j < linnad.get(linnad.keySet().toArray()[i]).length; j++) {
                System.out.println("\t"+ linnad.get(linnad.keySet().toArray()[i])[j]);

            }
        }

        System.out.println("------------------- for-each");

        for (Map.Entry riik : linnad.entrySet()) {
            System.out.println("Country: "  + riik.getKey() + "\nCities:");
            String[] linnadArray = linnad.get(riik.getKey());
            for (String linn : linnadArray) {
                System.out.println("\t" + linn);
            }

        }


    }
}
