package ee.bcs.valiit;

import java.util.Arrays;

public class Yl10 {
    public static void main(String[] args) {
        String[][] towns1 = {{"Tallinn", "Tartu", "Valga", "Võru"}, {"Stockholm", "Uppsala", "Lund", "Köping"}, {"Helsinki", "Espoo", "Hanko", "Jämsä"}};

        System.out.println(String.format("%s, %s, %s", Arrays.toString(towns1[0]), Arrays.toString(towns1[1]), Arrays.toString(towns1[2])));
        String[][] towns2 = new String[3][4];
        towns2[0][0] = "Tallinn";
        towns2[0][1] = "Tartu";
        towns2[0][2] = "Valga";
        towns2[0][3] = "Võru";
        towns2[1][0] = "Stockholm";
        towns2[1][1] = "Uppsala";
        towns2[1][2] = "Lund";
        towns2[1][3] = "Köping";
        towns2[2][0] = "Helsinki";
        towns2[2][1] = "Espoo";
        towns2[2][2] = "Hanko";
        towns2[2][3] = "Jämsä";
        System.out.println(String.format("%s, %s, %s", Arrays.toString(towns2[0]), Arrays.toString(towns2[1]), Arrays.toString(towns2[2])));

    }
}
