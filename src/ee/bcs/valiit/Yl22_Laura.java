package ee.bcs.valiit;

import java.util.HashMap;

public class Yl22_Laura {
    public static void main(String[] args) {
        HashMap<String, String[]> linnad = new HashMap<>();
        linnad.put("Estonia", new String[]{"Tallinn", "Tartu", "Valga", "Võru"});
        linnad.put("Sweden", new String[]{"Stockholm", "Uppsala", "Lund", "Köping"});
        linnad.put("Finland", new String[]{"Helsinki", "Espoo", "Hanko", "Jämsä"});

        for (int j = 0; j < linnad.size(); j++) {
            String country = linnad.keySet().toArray()[j].toString();
            System.out.println("Country: " + country);
            System.out.println("Cities: " );
            for (int i = 0; i < linnad.get(country).length; i++) {
                System.out.println("   " + linnad.get(country)[i]);
            }
        }

        for (String name : linnad.keySet()) {
            System.out.println("Country: " + name);
            System.out.println("Cities:");
            String[] linnArray = linnad.get(name);
            for (String city : linnArray) {
                System.out.println("   " + city);
            }
        }
    }

}
