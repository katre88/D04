package ee.bcs.valiit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Yl19 {


    public static void main(String[] args) {

        List<String> list = new ArrayList<>(Arrays.asList("Tallinn", "Tartu", "Pärnu", "Viljandi", "Narva"));
        list.add("Võru");
        list.set(2, "Tapa");
        list.remove("Võru");
        list.remove(list.get(list.size() - 1));
        System.out.println(String.format("%s, %s, %s", list.get(0), list.get(2), list.get(list.size() - 1)));

        for (String linn : list) {
            System.out.println(linn);
        }

        list.forEach(System.out::println);
    }

        /*list.add("Tallinn");
        list.add("Pärnu");
        list.add("Tartu");
        list.add("Narva");
        list.add("Viljandi");

        */

    }
