package ee.bcs.valiit;

public class Yl4 {
    public static void main(String[] args) {

        Integer arv = 3;
        switch (arv) {
            case 1:
                System.out.println("nõrk");
                break;
            case 2:
                System.out.println("mitterahuldav");
                break;
            case 3:
                System.out.println("rahuldav");
                break;
            case 4:
                System.out.println("hea");
                break;
            case 5:
                System.out.println("väga hea");
                break;
            default:
                System.out.println("Midagi läks valesti!");
        }

    }
}
