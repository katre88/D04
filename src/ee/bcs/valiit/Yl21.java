package ee.bcs.valiit;

import java.util.TreeSet;

public class Yl21 {
    public static void main(String[] args) {
        TreeSet<String> kollektsioon = new TreeSet();
        kollektsioon.add("Jüri");
        kollektsioon.add("Mari");
        kollektsioon.add("Juku");
        kollektsioon.add("Pille");
        kollektsioon.add("Kalle");
        kollektsioon.add("Malle");

        for (String item:kollektsioon) {
            System.out.println(item);
            
        }
        System.out.println();
        kollektsioon.forEach(System.out::println);

    }
}
